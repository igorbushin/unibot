#todo commands & keys

require 'serialport'

port = '/dev/ttyACM0'
rate = 9600
bits = 8
stop = 1
parity = SerialPort::NONE

channel = SerialPort::new port, rate, bits, stop, parity
while true
  command = gets
  break if command.include? 'q'
  channel.print command.chomp
  while true
    echo = channel.gets
    if echo
      puts 'echo: ' + echo
      break
    end
  end
end
channel.close
