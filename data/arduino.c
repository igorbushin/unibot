#include <Keyboard.h>

void press_alt_f4()
{
  Keyboard.press(KEY_LEFT_ALT);
  Keyboard.press(KEY_F4);
  delay(100);
  Keyboard.releaseAll();
}

void setup() 
{
  Serial.begin(9600);
  Keyboard.begin();
}

void loop() 
{
  if(Serial.available() > 0)
  {
    String input = Serial.readString();
    Serial.writeString(input);
  }

}
