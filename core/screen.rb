require 'opencv'
include OpenCV

def screenshot
  parent_directory = File.dirname(File.expand_path('..', __FILE__))
  file_name = parent_directory + '/data/screen.png'
  system 'import -window root ' + file_name
  CvMat.load file_name
end

=begin
  screen = screenshot
  window = GUI::Window.new 'window', CV_WINDOW_AUTOSIZE
  window.show screen
  GUI::wait_key
=end