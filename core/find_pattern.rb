require_relative 'mat'
require 'opencv'
include OpenCV

=begin
todo:
roi, masks
auto threshold
auto image scale
static vector patterns or pattern manager
=end

def find_pattern source, pattern_name, threshold, scale = 0.3
  parent_directory = File.dirname(File.expand_path('..', __FILE__))
  pattern = CvMat.load parent_directory + '/patterns/' + pattern_name
  pattern = pattern.resize_x scale
  source = source.resize_x scale
  match_map = source.match_template pattern, CV_TM_SQDIFF_NORMED
  matches = []
  match_map.width.times do |i|
    match_map.size.height.times do |j|
      value = (match_map.at j, i)[0]
      rect = CvRect.new i/scale, j/scale, pattern.size.width/scale, pattern.size.height/scale
      matches.push rect if value < threshold
    end
  end
  return matches
end

=begin
  require_relative 'draw_marks'
  screen = CvMat.load '../data/test.png'
  matches_4 = find_pattern screen, '2048_4.png', 0.01, 0.3
  screen = draw_rects screen, matches_4, CvColor::Green
  window = GUI::Window.new 'window', CV_WINDOW_NORMAL
  window.show screen
  GUI::wait_key
=end