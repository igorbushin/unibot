class String
  def is_alpha?
    !!match(/^[[:alpha:]]+$/)
  end

  def is_digit?
    !!match(/^\d+$/)
  end
end

=begin
  puts 'a'.is_alpha?
  puts '1'.is_alpha?
=end