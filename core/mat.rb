require 'opencv'
include OpenCV

class CvMat
  def resize_x scale
    size = CvSize.new self.width * scale, self.height * scale
    return self.resize size, CV_INTER_LINEAR
  end
end

=begin
  screen = CvMat.load '../data/test.png'
  screen = screen.resize_x 0.1
  window = GUI::Window.new 'window', CV_WINDOW_AUTOSIZE
  window.show screen
  GUI::wait_key
=end