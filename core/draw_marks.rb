require 'opencv'
include OpenCV

def draw_points source, points, color
  options = {:color => color, :thickness => 3}
  points.size.times do |i|
    source.circle! points[i], options
  end
  return source
end

def draw_rects source, rects, color
  options = {:color => color, :thickness => 3}
  rects.size.times do |i|
    source.rectangle! rects[i].top_left, rects[i].bottom_right, options
  end
  return source
end
