require_relative 'string'

def execute command
  if command.size > 12
    file_name = command[0, 6] + command[-6, 6]
  else
    file_name = command.clone
  end
  file_name.size.times do |i|
    next if file_name[i].is_alpha?
    if file_name[i] == ' ' then
      file_name[i] = '_'
    else
      file_name[i] = '*'
    end
  end
  parent_directory = File.dirname(File.expand_path('..', __FILE__))
  file_name = parent_directory + '/data/' + file_name
  system command + ' > ' + file_name
  file = File.open file_name, 'r'
  output = file.read
  file.close
  return output
end

#puts execute 'ls -la'
