require_relative 'execute'

#todo: set autoconnect false

def connect
  ssid = 'network'
  password = 'password'
  puts execute('nmcli device wifi')
  puts execute('nmcli connection delete ' + ssid)
  puts execute('nmcli device wifi connect ' + ssid + ' password ' + password + ' name ' + ssid)
  puts execute('nmcli connection modify ' + ssid + ' ipv4.dns 8.8.8.8 ipv4.ignore-auto-dns yes')
  puts execute('nmcli connection up ' + ssid)
  puts execute('ping -c4 ya.ru')
end

#connect
