require_relative 'core/screen'
require_relative 'core/draw_marks'
require_relative 'core/find_pattern'

max_power = 11
field_size = 6
thresholds = Array.new max_power, 0.01; thresholds[0] = 0.001
matches = Array.new max_power
field = Array.new field_size, (Array.new field_size, 0)
window = GUI::Window.new 'window', CV_WINDOW_NORMAL
while true
  screen = screenshot
  max_power.times do |i|
    value = 2 ** i
    matches[i] = find_pattern screen, '2048_' + value.to_s + '.png', thresholds[i]
  end
  max_power.times do |i|
    screen = draw_rects screen, matches[i], CvColor::Green
  end
  window.show screen
  key = GUI::wait_key 100
  break if key == 27
end

#vvvvvvvvvvvvvvvv LOW LEVEL vvvvvvvvvvvvvvvvvvvvvvvv

def reconstruct_field_by matches

end

def clock_wise_rotate field
  rotated_field = Array.new field_size, (Array.new field_size, 0)
  field_size.times do |i|
    field_size.times do |j|
      rotated_field[i][j] = field[j][field_size - 1 - i]
    end
  end
  return rotated_field
end

def make_move_left field
  field_size.times do |i|
    k = 0
    field_size.times do |j|
      if j < field_size - 1 && field[i][j] == field[i][j+1]
        field[i][j] *= 2;
        field[i][j+1] = 0;
      end
      if field[i][j]
        field[i][k] = field[i][j]
        k += 1
      end
    end
    while k < field_size
      field[i][k] = 0
      k += 1
    end
  end
end

# L D R U
def make_move_to field, x
  x.times do |i|
    field = clock_wise_rotate field
  end
  field = make_move_left
  (4 - x).times do |i|
    field = clock_wise_rotate field
  end
  return field
end

def count_scores field
  scores = 0
  field_size.times do |i|
    field_size.times do |j|
      scores += (field[i][j] - field[i+1][j]).abs if i < field_size - 1
      scores += (fiedl[i][j] - field[i][j+1]).abs if j < field_size - 1
    end
  end
  return scores
end

def find_best_move_direction field_0
  dir_0_scores = Array.new 4, 0
  4.times do |dir_0|
    field_1 = make_move_to field_0, dir_0
    empty_cells = 0
    field_size.times do |i|
      field_size.time do |j|
        next if field_1[i][j]
        field_1[i][j] = 2
        dir_1_scores = 0
        4.times do |dir_1|
          field_2 = make_move_to field_1, dir_1
          dir_1_scores = [dir_1_scores, (count_scores field_2)].max
        end
        field_1[i][j] = 0
        dir_0_scores[dir_0] += dir_1_scores
        empty_cells += 1
      end
    end
    dir_0_scores[dir_0] /= empty_cells
  end
  return dir_0_scores.index (dir_0_scores.max)
end
