#todo repair

require 'socket'
require 'uri'

def run port
  begin
    @socket = TCPServer.new port
    loop do
      client_socket = @socket.accept
      Thread.new client_socket, &method(:handle_request)
    end
  rescue Interrupt
    print 'Got interrupt.. '
  ensure
    if @socket
      print 'Close socket.. '
      @socket.close
    end
    puts 'Quit'
  end
end

def handle_request client_socket
  request = client_socket.readline
  verb = request[/^\w+/]
  url = request[/^\w+\s+(\S+)/, 1]
  uri = URI::parse url
  server_socket = TCPSocket.new(uri.host, uri.port)
  print request
  server_socket.write(request)
  loop do
    resend client_socket, server_socket
    resend server_socket, client_socket
    break
  end
  client_socket.close
  server_socket.close
end

def resend(socket_a, socket_b)
  content_length = 0
  loop do
    line = socket_a.readline
    if line =~ /^Content-Length:\s+(\d+)\s*$/
      content_length = $1.to_i
    elsif line.strip.empty?
      break
    end
    print line
    socket_b.write(line)
  end
  if content_length > 0
    content = socket_a.read(content_length)
    print "has content\n"
    socket_b.write(content)
  end
  print "\n"
end

run 8008
